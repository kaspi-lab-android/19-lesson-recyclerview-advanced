package com.example.advancedrecyclerview.restorescrollstate

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.model.Store
import com.example.advancedrecyclerview.nestedrecyclerview.StudentAdapter
import kotlinx.android.synthetic.main.activity_nested_recyclerview.*

private const val KEY_POSITION = "KEY_POSITION"
private const val KEY_OFFSET = "KEY_OFFSET"

class RestoreScrollStateActivity : AppCompatActivity(R.layout.activity_nested_recyclerview) {
    private var adapter: StudentAdapter? = null
    private var layoutManager: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setList()
        if (savedInstanceState != null) {
            recyclerView?.post {
                val itemPosition = savedInstanceState.getInt(KEY_POSITION, 0)
                val itemOffset = savedInstanceState.getInt(KEY_OFFSET, 0)
                layoutManager?.scrollToPositionWithOffset(itemPosition, itemOffset)
            }
        }
    }

    private fun setList() {
        adapter = StudentAdapter()
        adapter?.setHasStableIds(true)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        adapter?.setItems(Store.getStudentList())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        val firstItemPosition = layoutManager?.findFirstVisibleItemPosition() ?: 0
        val top = layoutManager?.findViewByPosition(firstItemPosition)?.top ?: 0
        outState.putInt(KEY_POSITION, firstItemPosition)
        outState.putInt(KEY_OFFSET, top)
        super.onSaveInstanceState(outState)
    }
}