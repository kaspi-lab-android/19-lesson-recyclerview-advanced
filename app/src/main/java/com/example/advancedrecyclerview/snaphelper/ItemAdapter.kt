package com.example.advancedrecyclerview.snaphelper

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.model.Student

class ItemAdapter: RecyclerView.Adapter<ItemViewHolder>() {

    private val data = mutableListOf<Student>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ItemViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount(): Int =
        data.size

    override fun getItemId(position: Int): Long =
        data[position].id

    fun setItems(list: List<Student>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }
}