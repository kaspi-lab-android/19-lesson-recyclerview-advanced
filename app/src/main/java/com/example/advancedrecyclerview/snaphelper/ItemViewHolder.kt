package com.example.advancedrecyclerview.snaphelper

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.model.Student
import kotlinx.android.synthetic.main.item_snap.view.*

class ItemViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.item_snap, parent, false)) {

    private val idTextView = itemView.idTextView
    private val firstNameTextView = itemView.firstNameTextView
    private val lastNameTextView = itemView.lastNameTextView

    fun onBind(student: Student) {
        idTextView.text = student.id.toString()
        firstNameTextView.text = student.firstname
        lastNameTextView.text = student.lastname
    }
}