package com.example.advancedrecyclerview.snaphelper

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.model.Store
import kotlinx.android.synthetic.main.activity_nested_recyclerview.*


class SnapHelperActivity: AppCompatActivity(R.layout.activity_snaphelper) {
    private var adapter: ItemAdapter? = null
    private var snapPosition = RecyclerView.NO_POSITION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = ItemAdapter()
        adapter?.setHasStableIds(true)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerView.layoutManager = layoutManager

        val studentList = Store.getStudentList()
        adapter?.setItems(studentList)

        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(recyclerView)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                snapHelper.findSnapView(layoutManager)?.let {
                    val position = layoutManager.getPosition(it)
                    if (snapPosition != position) {
                        snapPosition = position
                        // invoke some callback
                    }
                }
            }
        })
    }
}