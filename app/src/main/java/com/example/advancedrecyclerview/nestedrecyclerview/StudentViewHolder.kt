package com.example.advancedrecyclerview.nestedrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.diffutil.StudentPayload
import com.example.advancedrecyclerview.model.Class
import com.example.advancedrecyclerview.model.Student
import kotlinx.android.synthetic.main.item_student.view.*

class StudentViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.item_student, parent, false)) {

    private val idTextView = itemView.idTextView
    private val firstNameTextView = itemView.firstNameTextView
    private val lastNameTextView = itemView.lastNameTextView
    private val classGradesRecyclerView = itemView.classGradesRecyclerView

    fun onBind(student: Student, viewPool: RecyclerView.RecycledViewPool) {
        updateId(student.id)
        updateFirstName(student.firstname)
        updateLastName(student.lastname)
        updateClasses(student.classes, viewPool)
    }

    fun onBind(student: Student, viewPool: RecyclerView.RecycledViewPool, fields: Set<*>?) {
        fields?.forEach {
            when (it) {
                StudentPayload.ID -> updateId(student.id)
                StudentPayload.FIRST_NAME -> updateFirstName(student.firstname)
                StudentPayload.LAST_NAME -> updateLastName(student.lastname)
                StudentPayload.CLASSES -> updateClasses(student.classes, viewPool)
            }
        }
    }

    private fun updateId(id: Long) {
        idTextView.text = id.toString()
    }

    private fun updateFirstName(firstName: String) {
        firstNameTextView.text = firstName
    }

    private fun updateLastName(lastName: String) {
        lastNameTextView.text = lastName
    }

    private fun updateClasses(classes: List<Class>, viewPool: RecyclerView.RecycledViewPool) {
        val childLayoutManager =
            LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
        childLayoutManager.initialPrefetchItemCount = 4

        classGradesRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = childLayoutManager
            val adapter = ClassAdapter()
            adapter.setHasStableIds(true)
            adapter.setItems(classes)
            this.adapter = adapter
            setRecycledViewPool(viewPool)
        }
    }
}