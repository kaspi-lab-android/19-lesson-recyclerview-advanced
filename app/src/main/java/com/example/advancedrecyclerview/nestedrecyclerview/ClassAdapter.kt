package com.example.advancedrecyclerview.nestedrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.model.Class

class ClassAdapter: RecyclerView.Adapter<ClassViewHolder>() {

    private val data = mutableListOf<Class>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ClassViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ClassViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount(): Int =
        data.size

    override fun getItemId(position: Int): Long =
        data[position].id

    fun setItems(list: List<Class>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }
}