package com.example.advancedrecyclerview.nestedrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.model.Student

class StudentAdapter: RecyclerView.Adapter<StudentViewHolder>() {

    private val data = mutableListOf<Student>()
    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return StudentViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.onBind(data[position], viewPool)
    }

    override fun getItemCount(): Int =
        data.size

    override fun getItemId(position: Int): Long =
        data[position].id

    fun setItems(list: List<Student>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }
}