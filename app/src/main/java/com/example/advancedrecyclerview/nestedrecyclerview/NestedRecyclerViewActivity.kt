package com.example.advancedrecyclerview.nestedrecyclerview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.model.Store
import kotlinx.android.synthetic.main.activity_nested_recyclerview.*

class NestedRecyclerViewActivity : AppCompatActivity(R.layout.activity_nested_recyclerview) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val adapter = StudentAdapter()
        adapter.setHasStableIds(true)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        val studentList = Store.getStudentList()
        adapter.setItems(studentList)
    }
}