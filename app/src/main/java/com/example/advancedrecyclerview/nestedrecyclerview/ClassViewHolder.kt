package com.example.advancedrecyclerview.nestedrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.model.Class
import kotlinx.android.synthetic.main.item_class.view.*

class ClassViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.item_class, parent, false)) {

    private val container = itemView.container
    private val nameTextView = itemView.nameTextView
    private val timeTextView = itemView.timeTextView

    fun onBind(cls: Class) {
        nameTextView.text = cls.name
        timeTextView.text = cls.time
        container.setOnClickListener {
            Toast.makeText(itemView.context, "${cls.name} clicked!", Toast.LENGTH_SHORT).show()
        }
    }
}