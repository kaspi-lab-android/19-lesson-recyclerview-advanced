package com.example.advancedrecyclerview.model

data class Class(
    val id: Long,
    val name: String,
    val time: String
)