package com.example.advancedrecyclerview.model

data class Student(
    val id: Long,
    val firstname: String,
    val lastname: String,
    val classes: List<Class>
)