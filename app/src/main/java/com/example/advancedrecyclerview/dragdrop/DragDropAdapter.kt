package com.example.advancedrecyclerview.dragdrop

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import kotlinx.android.synthetic.main.item_drag.view.*

class DragDropAdapter(private val itemTouchDelegate: ItemTouchDelegate) :
    RecyclerView.Adapter<DragDropAdapter.MainRecyclerViewHolder>() {
    private var emojis = listOf(
        "😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "☺️", "😊", "😇", "🙂", "🙃", "😉"
    ).toMutableList()

    fun moveItem(from: Int, to: Int) {
        val fromEmoji = emojis[from]
        emojis.removeAt(from)
        if (to < from) {
            emojis.add(to, fromEmoji)
        } else {
            emojis.add(to - 1, fromEmoji)
        }
    }

    override fun getItemCount(): Int = emojis.size

    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {
        holder.setText(emojis[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainRecyclerViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_drag, parent, false)
        val viewHolder = MainRecyclerViewHolder(itemView)

        viewHolder.itemView.handleView.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                itemTouchDelegate.startDragging(viewHolder)
            }
            return@setOnTouchListener true
        }

        return viewHolder
    }

    class MainRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setText(text: String) {
            itemView.textView.text = text
        }
    }
}