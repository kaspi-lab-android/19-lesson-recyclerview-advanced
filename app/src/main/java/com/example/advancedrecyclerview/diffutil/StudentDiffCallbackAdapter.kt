package com.example.advancedrecyclerview.diffutil

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.model.Student
import com.example.advancedrecyclerview.nestedrecyclerview.StudentViewHolder

class StudentDiffCallbackAdapter: RecyclerView.Adapter<StudentViewHolder>() {

    private val data = mutableListOf<Student>()
    private val viewPool = RecyclerView.RecycledViewPool()
    private val diffCallback = StudentDiffCallback()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return StudentViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.onBind(data[position], viewPool)
    }

    override fun onBindViewHolder(
        holder: StudentViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            holder.onBind(data[position], viewPool, payloads.first() as? Set<*>)
        }
    }

    override fun getItemCount(): Int =
        data.size

    override fun getItemId(position: Int): Long =
        data[position].id

    fun setItemsWithDiffCallback(list: List<Student>) {
        diffCallback.setItems(data, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback, false)
        data.clear()
        data.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }
}