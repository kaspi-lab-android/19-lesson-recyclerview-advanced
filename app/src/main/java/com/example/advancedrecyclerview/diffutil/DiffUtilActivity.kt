package com.example.advancedrecyclerview.diffutil

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.advancedrecyclerview.R
import com.example.advancedrecyclerview.model.Class
import com.example.advancedrecyclerview.model.Store
import kotlinx.android.synthetic.main.activity_diffutil.*
import kotlinx.android.synthetic.main.activity_nested_recyclerview.recyclerView

class DiffUtilActivity : AppCompatActivity(R.layout.activity_diffutil) {
    private var adapter: StudentDiffCallbackAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setList()
        setButtons()
    }

    private fun setButtons() {
        button1.setOnClickListener {
            val studentList = Store.getStudentList().toMutableList()
            // change first name
            studentList[0] = studentList[0].copy(firstname = "David")
            adapter?.setItemsWithDiffCallback(studentList)
        }
        button2.setOnClickListener {
            val studentList = Store.getStudentList().toMutableList()
            val classes = studentList[0].classes.toMutableList()
            // prepend a class
            classes.add(0, Class(0, "Law", "12:00"))
            studentList[0] = studentList[0].copy(classes = classes)
            adapter?.setItemsWithDiffCallback(studentList)
        }
    }

    private fun setList() {
        val studentList = Store.getStudentList()

        adapter = StudentDiffCallbackAdapter()
        adapter?.setHasStableIds(true)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)

        val itemAnimator = DefaultItemAnimator()
        itemAnimator.supportsChangeAnimations = false
        recyclerView.itemAnimator = itemAnimator

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        adapter?.setItemsWithDiffCallback(studentList)
    }
}