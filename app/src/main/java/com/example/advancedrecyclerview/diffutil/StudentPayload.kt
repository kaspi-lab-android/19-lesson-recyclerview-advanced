package com.example.advancedrecyclerview.diffutil

enum class StudentPayload {
    ID,
    FIRST_NAME,
    LAST_NAME,
    CLASSES
}