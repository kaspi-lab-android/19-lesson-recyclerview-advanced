package com.example.advancedrecyclerview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.advancedrecyclerview.diffutil.DiffUtilActivity
import com.example.advancedrecyclerview.dragdrop.DragDropActivity
import com.example.advancedrecyclerview.nestedrecyclerview.NestedRecyclerViewActivity
import com.example.advancedrecyclerview.restorescrollstate.RestoreScrollStateActivity
import com.example.advancedrecyclerview.snaphelper.SnapHelperActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        nestedButton.setOnClickListener {
            startActivity(Intent(this, NestedRecyclerViewActivity::class.java))
        }

        diffUtilButton.setOnClickListener {
            startActivity(Intent(this, DiffUtilActivity::class.java))
        }

        restoreButton.setOnClickListener {
            startActivity(Intent(this, RestoreScrollStateActivity::class.java))
        }

        snapHelperButton.setOnClickListener {
            startActivity(Intent(this, SnapHelperActivity::class.java))
        }

        dragDropButton.setOnClickListener {
            startActivity(Intent(this, DragDropActivity::class.java))
        }
    }
}